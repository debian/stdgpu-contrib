REPOSITORY MOVED
================

The stdgpu-contrib source package is now managed in the master-contrib branch
of the stdgpu_ repository.

.. _stdgpu: https://salsa.debian.org/debian/stdgpu


